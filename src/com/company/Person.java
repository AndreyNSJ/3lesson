package com.company;

public class Person {
    private String name = "";
    private String surname = "";
    private String patronymic = "";
    private String fio;

    public Person(String name, String surname, String patronymic){
        this.name=name;
        this.surname=surname;
        this.patronymic=patronymic;
    }


    public Person(String fio){
        this.fio = fio;
        int i = 0, numOfChar = 0;
        do{
            surname = surname + fio.charAt(numOfChar);
            numOfChar++;
            i++;
            System.out.println(i);
        } while (fio.charAt(numOfChar) != ' ');
        numOfChar++;
        i++;
        do{
            name = name + fio.charAt(numOfChar);
            numOfChar++;
            i++;
            System.out.println(i);
        } while (fio.charAt(numOfChar) != ' ');
        numOfChar++;
        i++;
        do{
            patronymic = patronymic + fio.charAt(numOfChar);
            numOfChar++;
            i++;
            System.out.println(i);
        } while (i != fio.length());
    }

    public void getFIO(){
        System.out.println(name + " " + surname + " " + patronymic);
    }
}
